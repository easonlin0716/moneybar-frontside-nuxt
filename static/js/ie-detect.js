;(function msieversion() {
  var ua = window.navigator.userAgent
  var msie = ua.indexOf('MSIE ')

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    // If Internet Explorer
    alert('很抱歉，網站不支援 Internet Explorer，請改用其他瀏覽器')
    window.history.back()
    throw new Error('很抱歉，網站不支援 Internet Explorer，請改用其他瀏覽器')
  } // If another browser, return false
  else {
    return true
  }
})()
