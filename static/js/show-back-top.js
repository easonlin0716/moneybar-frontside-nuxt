;(function() {
  const invokeMidScreen = 992
  const showBackTop = document.querySelector('#showbacktop')
  // primary navigation slide-in effect
  if (document.body.clientWidth > invokeMidScreen) {
    let previousTop = 0
    const headerHeight = document.querySelector('#showbacktop').clientHeight
    document.addEventListener('scroll', () => {
      const currentTop = window.pageYOffset
      const minHeader = 120
      // check if user is scrolling up
      if (currentTop < previousTop) {
        // if scrolling up...
        if (
          currentTop > minHeader &&
          showBackTop.classList.contains('is-fixed')
        ) {
          showBackTop.classList.add('is-visible')
        } else {
          showBackTop.classList.remove('is-visible', 'is-fixed')
        }
      } else if (currentTop > previousTop) {
        // if scrolling down...
        showBackTop.classList.remove('is-visible')
        if (
          currentTop > headerHeight &&
          !showBackTop.classList.contains('is-fixed')
        )
          showBackTop.classList.add('is-fixed')
      }
      previousTop = currentTop
    })
  }
})()
