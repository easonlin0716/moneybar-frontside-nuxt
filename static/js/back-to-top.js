/* eslint-disable */
;(function() {
  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //grab the "back to top" link
    $back_to_top = $('.back-top')
  //hide or show the "back to top" link
  $(window).scroll(function() {
    $(this).scrollTop() > offset
      ? $back_to_top.addClass('backtop-is-visible')
      : $back_to_top.removeClass('backtop-is-visible backtop-fade-out')
    if ($(this).scrollTop() > offset_opacity) {
      $back_to_top.addClass('backtop-fade-out')
    }
  })
  // Material-scrolltop
  function mScrollTop(element, settings) {
    var _ = this,
      breakpoint
    var scrollTo = 0
    _.btnClass = '.material-scrolltop'
    _.revealClass = 'reveal'
    _.btnElement = $(_.btnClass)
    _.initial = {
      revealElement: 'body',
      revealPosition: 'top',
      padding: 0,
      duration: 600,
      easing: 'swing',
      onScrollEnd: false
    }
    _.options = $.extend({}, _.initial, settings)
    _.revealElement = $(_.options.revealElement)
    breakpoint =
      _.options.revealPosition !== 'bottom'
        ? _.revealElement.offset().top
        : _.revealElement.offset().top + _.revealElement.height()
    scrollTo = element.offsetTop + _.options.padding
    $(document).scroll(function() {
      if (breakpoint < $(document).scrollTop()) {
        _.btnElement.addClass(_.revealClass)
      } else {
        _.btnElement.removeClass(_.revealClass)
      }
    })
    _.btnElement.click(function() {
      var trigger = true
      $('html, body').animate(
        {
          scrollTop: scrollTo
        },
        _.options.duration,
        _.options.easing,
        function() {
          if (trigger) {
            // Fix callback triggering twice on chromium
            trigger = false
            var callback = _.options.onScrollEnd
            if (typeof callback === 'function') {
              callback()
            }
          }
        }
      )
      return false
    })
  }
  $.fn.materialScrollTop = function() {
    var _ = this,
      opt = arguments[0],
      l = _.length,
      i = 0
    if (typeof opt == 'object' || typeof opt == 'undefined') {
      _[i].materialScrollTop = new mScrollTop(_[i], opt)
    }
    return _
  }
  $('body').materialScrollTop({
    // Scroll to the top of <body> element ...
    revealElement: 'header', // Reveal button when scrolling over <header><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> ...
    revealPosition: 'bottom', // ... and do it at the end of </header> element
    duration: 1000 // Animation will run 1000 ms
  })
})()
