import Swal from 'sweetalert2'

export default function({ app, redirect }) {
  if (!app.$cookies.get('memberAccount')) {
    Swal.fire('請先登入')
    return redirect('/login')
  }
}
