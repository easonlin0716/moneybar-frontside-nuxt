export const state = () => ({
  authRegisterData: {}
})

export const mutations = {
  SET_AUTH_REGISTER_DATA(state, payload) {
    return (state.authRegisterData = payload)
  }
}
