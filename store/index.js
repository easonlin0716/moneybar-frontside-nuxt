import tokenGenerator from '@/utils/tokenGenerator'
import { Toast } from '@/utils/Toast'

export const state = () => ({
  user: {
    watchlists: [],
    subscriptions: [] // 使用者的訂閱 plan_id 陣列
  },
  spgInfo: {},
  deviceType: null
})

export const getters = {
  isAuthenticated: (state) => {
    return !!state.user.MemberNo
  },
  memberId: (state) => {
    return null || state.user.MemberNo
  },
  memberAccount: (state) => {
    return null || state.user.memberAccount
  },
  userSubscriptions: (state) => {
    return state.user.subscriptions || []
  }
}

export const mutations = {
  SET_AUTH: (state, payload) => {
    localStorage.setItem('memberId', payload.memberId)
    localStorage.setItem('memberAccount', payload.memberAccount)
  },
  SET_USER: (state, payload) => {
    state.user = {
      ...state.user,
      ...payload
    }
  },
  SET_USER_WATCHLIST: (state, payload) => {
    state.user.watchlists = payload
  },
  PUSH_USER_WATCHLIST: (state, payload) => {
    state.user.watchlists.push(payload)
  },
  SPLICE_USER_WATCHLIST: (state, payload) => {
    const i = state.user.watchlists.indexOf(payload)
    state.user.watchlists.splice(i, 1)
  },
  SET_USER_SUBSCRIPTIONS: (state, payload) => {
    state.user.subscriptions = payload
  },
  CLEAR_USER: (state) => {
    state.user = { watchlists: [], subscriptions: [] }
  },
  SET_SPG_INFO: (state, payload) => {
    state.spgInfo = payload
  },
  SET_USER_ORDERSTATE: (state) => {
    state.user.orderstate = state.user.orderstate === 1 ? 0 : 1
  },
  SET_DEVICE_TYPE: (state, payload) => {
    state.deviceType = payload
  }
}

export const actions = {
  async nuxtServerInit(store, { app }) {
    const cookies = app.$cookies.getAll()
    if (cookies.memberId && cookies.memberAccount) {
      app.$axios.defaults.headers.sKey = cookies.sKey || ''
      const { data } = await this.$axios.post('member/info', {
        member_account: cookies.memberAccount,
        token: tokenGenerator()
      })
      const userData = data.content.member_data
      store.state.user.memberAccount = cookies.memberAccount
      return store.commit('SET_USER', userData)
    }
  },
  async FETCH_USER_WATCHLIST({ state, commit }) {
    const MemberNo = this.$cookies.get('memberId')
    const { data } = await this.$axios.post('member/watch/idlist', {
      MemberNo,
      token: tokenGenerator()
    })
    return commit('SET_USER_WATCHLIST', data.content)
  },
  async ADD_USER_WATCHLIST({ state, commit }, etfPID) {
    const { data } = await this.$axios.post('member/watch/add', {
      MemberNo: state.user.MemberNo,
      ETFPerformanceID: etfPID,
      token: tokenGenerator()
    })
    if (data.err_msg === 'success' && data.status === '000') {
      Toast.fire({
        icon: 'success',
        title: '觀察清單加入成功！'
      })
      return commit('PUSH_USER_WATCHLIST', etfPID)
    }
  },
  async DELETE_USER_WATCHLIST({ state, commit }, etfPID) {
    const { data } = await this.$axios.post('member/watch/remove', {
      MemberNo: state.user.MemberNo,
      ETFPerformanceID: etfPID,
      token: tokenGenerator()
    })
    if (data.err_msg === 'success' && data.status === '000') {
      Toast.fire({
        title: '刪除成功！',
        icon: 'success'
      })
    }
    return commit('SPLICE_USER_WATCHLIST', etfPID)
  },
  /**
   * 透過 MemberNo 取得該會員有訂閱的所有 plan_id
   */
  async FETCH_USER_SUBSCRIPTIONS({ state, commit }) {
    const { data } = await this.$axios.post('plan/dif', {
      MemberNo: state.user.MemberNo,
      token: tokenGenerator()
    })
    return commit('SET_USER_SUBSCRIPTIONS', data.content)
  },
  async TOGGLE_USER_ORDERSTATE({ state, commit }) {
    await this.$axios.post('member/edit', {
      member_id: state.user.MemberNo,
      member_account: state.user.memberAccount,
      orderstate: state.user.orderstate === 1 ? 0 : 1,
      token: tokenGenerator()
    })
    return commit('SET_USER_ORDERSTATE', null)
  }
}
