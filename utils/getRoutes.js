require('dotenv').config()
const dayjs = require('dayjs')
const axios = require('axios')
const md5 = require('md5')
const utc = require('dayjs/plugin/utc') // dependent on utc plugin
const timezone = require('dayjs/plugin/timezone')
const menuData = require('../assets/json/menu.config.json')
dayjs.extend(utc)
dayjs.extend(timezone)
const tokenGenerator = function() {
  const YYYYMMDDHH = dayjs()
    .tz('Asia/Taipei')
    .format('YYYYMMDDHH')
  const minute =
    Math.floor(
      Number(
        dayjs()
          .tz('Asia/Taipei')
          .format('mm')
      ) / 5
    ) * 5
  const mm = minute > 9 ? minute : '0' + minute
  const result = `moneybar_${YYYYMMDDHH}${mm}`
  return md5(result)
}
const getLink = function(arr, result) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].link) {
      result.push(arr[i].link)
    }
    if (arr[i].subMenu) {
      getLink(arr[i].subMenu, result)
    }
  }
  return null
}

module.exports = async function getRoutes() {
  console.log(`getRoutes generates at: ${new Date()}`)
  const menuRoutes = []
  getLink(menuData.menu, menuRoutes)
  const dynamicRoutes = []
  const { data } = await axios.post(`${process.env.API_URL}siteMap`, {
    token: tokenGenerator()
  })
  const newsLinkArray = data.site_map
  dynamicRoutes.push(...newsLinkArray)
  return [
    ...dynamicRoutes.map((url) => ({ url, lastmod: new Date() })),
    ...menuRoutes.map((url) => ({ url, lastmod: new Date() }))
  ]
}
