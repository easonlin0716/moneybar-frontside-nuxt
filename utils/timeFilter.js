/**
 * Returns filtered time or empty string if time is undefined, null or empty string
 *
 * @param {string} t time string e.g. 2020-08-17 10:00:00
 * @returns {string} filtered time or empty string
 */
export default function(t) {
  if (!t) return ''
  return t.slice(0, 16).replace(/-/g, '/')
}
