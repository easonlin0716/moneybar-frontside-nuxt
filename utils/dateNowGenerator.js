/**
 * @param {number} dateNow Date.now() returns time value
 * @returns {string} returns ISO string format but is local time
 */
export default function(dateNow) {
  const tZOffset = new Date().getTimezoneOffset() * 60000
  return new Date(dateNow - tZOffset)
    .toISOString()
    .replace(/-/g, '/')
    .replace('T', ' ')
    .slice(0, 16)
}
