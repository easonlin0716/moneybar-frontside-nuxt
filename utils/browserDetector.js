const ua = navigator.userAgent.toLowerCase()
module.exports = {
  isLineApp: ua.includes('line'),
  isFBApp: ua.includes('fbav'),
  isWeixinApp: ua.includes('micromessenger')
}
// export const isLineApp = ua.includes('line')
// export const isFBApp = ua.includes('fbav')
// export const isWeixinApp = ua.includes('micromessenger')
