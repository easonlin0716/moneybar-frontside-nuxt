/**
 * An imgPlaceholder preventing backend api passing only image path
 *
 * @param {string} s image string e.g. image.jpg, image.png
 * @param {string} type image type and it will refer to folder's name, default would be 'small'
 * @returns {string} passed string if contains image file name, fake image if not
 */
export default function(s, type = 'small') {
  if (/jpg|jpeg|png|JPG|PNG|JPEG/gi.test(s)) return s
  return `/img/news-list-placeholder/${type}/P${Math.floor(
    Math.random() * 26
  )}.jpg`
}
