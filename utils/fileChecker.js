/**
 * generate size string to number
 * @param {string} s size string (e.g., '10kb')
 * @returns {number} size number
 */
const limitGenerator = function(s) {
  /*
   * 接收數字加上容量單位的字串，轉換成純數字的 bytes
   */
  const unit = s.slice(-2).toLowerCase()
  if (unit !== 'kb' && unit !== 'mb' && unit !== 'gb') {
    throw new Error('Unit must be kb, mb or gb.')
  }
  const amount = Number(s.slice(0, -2))
  let limit = 0
  if (unit === 'kb') {
    limit = amount * 1024
    return limit
  }
  if (unit === 'mb') {
    limit = amount * 1024 * 1024
    return limit
  }
  if (unit === 'gb') {
    limit = amount * 1024 * 1024 * 1024
    return limit
  }
  return null
}
/**
 * check if file's size is bigger than limit
 * @param {string} file
 * @param {number} limit limit generate from limitGenerator
 * @param {object} el if file's size is bigger than limit, clear el's value
 * @returns {boolean} returns true if file's size is not bigger than limit
 */
const fileChecker = function(file, limit, el) {
  /*
   * target 為 event.target.files[0]；limit 為容量；el 為目標 element
   */
  const bytes = limitGenerator(limit)
  if (file.size > bytes) {
    // 如果容量超過 limit，清除 element 中存放的檔案
    el.value = ''
    return false
  }
  return true
}
export default fileChecker
