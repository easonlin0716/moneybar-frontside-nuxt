/**
 *
 * @param {string} primaryCategory primaryCategory to be filtered
 * @param {string} subCategory subCategory to be filtered
 * @param {object} menu menu to filter
 * @returns {object} links
 */

export default function(primaryCategory, subCategory, menu) {
  let primaryCategoryLink = '/'
  let subCategoryLink = '/'
  for (let i = 0; i < menu.length; i++) {
    if (primaryCategory.toLowerCase() === menu[i].title.toLowerCase()) {
      primaryCategoryLink = menu[i].link
      for (let j = 0; j < menu[i].subMenu.length; j++) {
        if (
          subCategory.toLowerCase() === menu[i].subMenu[j].title.toLowerCase()
        ) {
          subCategoryLink = menu[i].subMenu[j].link
          return { primaryCategoryLink, subCategoryLink }
        }
      }
    }
  }
  return { primaryCategoryLink, subCategoryLink }
}
