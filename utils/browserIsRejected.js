const { isLineApp, isFBApp, isWeixinApp } = require('@/utils/browserDetector')
module.exports = function() {
  const rejectHtmlMsg = (browserType) =>
    `<p>${browserType} 瀏覽器不支援第三方登入，</p><p>請使用其他瀏覽器操作（Chrome、Safari）。</p>`
  if (isLineApp) {
    this.$Swal.fire({ icon: 'info', html: rejectHtmlMsg('Line') })
    return true
  }
  if (isFBApp) {
    this.$Swal.fire({ icon: 'info', html: rejectHtmlMsg('FB') })
    return true
  }
  if (isWeixinApp) {
    this.$Swal.fire({ icon: 'info', html: rejectHtmlMsg('微信') })
    return true
  }
  return false
}
