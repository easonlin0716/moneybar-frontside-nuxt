const dayMap = {
  Mon: 'Monday',
  Tue: 'Tuesday',
  Wed: 'Wednesday',
  Thu: 'Thursday',
  Fri: 'Friday',
  Sat: 'Saturday',
  Sun: 'Sunday'
}

const monthMap = {
  Jan: 'January',
  Feb: 'February',
  Mar: 'March',
  Apr: 'April',
  May: 'May',
  Jun: 'June',
  Jul: 'July',
  Aug: 'August',
  Sep: 'September',
  Oct: 'October',
  Nov: 'November',
  Dec: 'December'
}
/**
 * @returns weekday, month day, fullyear
 */
const dateGenerator = function() {
  const dateArr = new Date().toDateString().split(' ')
  let [weekday, month] = ['', '']
  for (const key in dayMap) {
    if (key === dateArr[0]) {
      weekday = dayMap[key]
    }
  }
  for (const key in monthMap) {
    if (key === dateArr[1]) {
      month = monthMap[key]
    }
  }
  return `${weekday}, ${month} ${dateArr[2]}, ${dateArr[3]}`
}

export default dateGenerator
