/* eslint-disable */
const citySuffix = ['市', '縣', '島']
const areaSuffix = ['區', '鄉', '鎮', '市']
/**
 * generate address into city, area and address suffix
 * @param {string} input total address
 * @returns {object} city, area and address suffix
 */
const addressGenerator = function(input) {
  input = input.replace(/^\d*/gi, '')
  let i = 1,
    city,
    area,
    address
  const addressArray = input.split('')
  for (let i = 0; i < addressArray.length; i++) {
    if (citySuffix.indexOf(addressArray[i]) !== -1 && !city) {
      city = addressArray.splice(0, i + 1).join('')
      i = 0
    }
    if (areaSuffix.indexOf(addressArray[i]) !== -1 && !area) {
      area = addressArray.splice(0, i + 1).join('')
      i = 0
    }
  }
  address = addressArray.join('')
  return { city, area, address }
}

export default addressGenerator
