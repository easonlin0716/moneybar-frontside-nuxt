/**
 * Check if two arrays have at least one same value
 *
 * @param {Array} arr1 passed first array
 * @param {Array} arr2 passed second array
 * @return {Boolean} returns true if arr1 and arr2 have at least one same value
 */
export default function(arr1, arr2) {
  for (let i = 0; i < arr2.length; i++) {
    if (arr1.includes(arr2[i])) return true
  }
  return false
}
