export default function(s) {
  if (s.match(/<p>/g) !== null) return s
  let preset = ''
  if (s.includes('文/moneybar智能小編')) {
    preset = '<p>文/moneybar智能小編</p>'
    s = s.replace('文/moneybar智能小編', '')
  }
  const separated = s.split(/。(?!;)|<div>|<\/div>/g)
  console.log(separated)
  for (let i = 0; i < separated.length; i++) {
    if (separated[i]) {
      separated[i] = '<p>' + separated[i] + '。</p>'
    }
  }
  return '<div>' + preset + separated.join('') + '</div>'
}
