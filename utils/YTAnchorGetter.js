/* eslint-disable */
/**
 * return youtube anchor for embed tag to display youtube videos
 * @param {string} YTWebsite youtube website for validation
 * @returns {string} returns empty string if param is not a valid youtube website
 * @returns {string} youtube anchor for embed tag to display youtube video
 */
function YTAnchorGetter(YTWebsite) {
  let YTAnchor = ''
  const reg = /&feature=youtu.be|\?rel=0&autoplay=\d?/
  YTWebsite = YTWebsite.replace(reg, '')
  for (let i = 0; i <= YTWebsite.length; i++) {
    let letter = YTWebsite.charAt(YTWebsite.length - i)
    if (letter === '=' || letter === '/') {
      YTAnchor = YTAnchor.split('')
        .reverse()
        .join('')
      return YTAnchor
    } else {
      YTAnchor += letter
    }
  }
}

export default YTAnchorGetter
