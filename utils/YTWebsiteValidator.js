/* eslint-disable */
/**
 * validate if text is a valid youtube video website
 * @param {string} text youtube video website
 * @returns {boolean} returns true if it is
 */
function YTWebsiteValidator(text) {
  if (!text) return null
  const regex = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/
  return regex.test(text)
}

export default YTWebsiteValidator
