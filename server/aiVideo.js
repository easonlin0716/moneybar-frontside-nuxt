const express = require('express')
const app = express()
const request = require('request')
require('dotenv').config()
const API_URL = process.env.API_URL || 'https://api.moneybar.com.tw/public/api/'

app.get('/rss', (req, res) => {
  request(`${API_URL}RssFeed/AiVideo`, function(error, response, body) {
    res.set('Content-Type', 'text/xml')
    res.send(body)
  })
})

module.exports = app
