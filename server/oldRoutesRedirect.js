/**
 * This is designed to support old link redirect for SEO usage
 */
const express = require('express')
const app = express()

app.get('/About', (req, res) => {
  return res.redirect(301, '/aboutus')
})

app.get('/Anakin', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/14')
})

app.get('/Anakin/Info', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/14')
})

app.get('/article-*', (req, res) => {
  return res.redirect(301, `/article/${req.path.replace('/article-', '')}`)
})

app.get('/experts.php', (req, res) => {
  return res.redirect(301, '/vip')
})

app.get('/expertsDetail.php', (req, res) => {
  console.log(req.query)
  const expertID = req.query.expert_id
  if (!expertID) return res.redirect('/error')
  return res.redirect(301, `/blogs/moneyvip/${expertID}`)
})

app.get('/faq.php', (req, res) => {
  return res.redirect(301, '/faq')
})

app.get('/goldenlaw/*', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/45')
})

app.get('/login.php', (req, res) => {
  return res.redirect(301, '/login')
})

app.get('/missQ', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/17')
})

app.get('/Ousimai', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/11')
})

app.get('/plans.php', (req, res) => {
  return res.redirect(301, '/plans')
})

app.get('/currency/*', (req, res) => {
  return res.redirect(301, '/investbar/exchange')
})

app.get('/YeFang/', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/31')
})

app.get('/YeFang/Info', (req, res) => {
  return res.redirect(301, '/blogs/moneyvip/31')
})

module.exports = app
