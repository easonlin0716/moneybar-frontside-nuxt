const express = require('express')
const app = express()
const request = require('request')

app.get('/rss', (req, res) => {
  request(
    'https://api.moneybar.com.tw/public/api/RssFeed/LineTodayVideo',
    function(error, response, body) {
      res.set('Content-Type', 'text/xml')
      res.send(body)
    }
  )
})

module.exports = app
