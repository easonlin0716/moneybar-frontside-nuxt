const getRoutes = require('./utils/getRoutes')
require('dotenv').config()

module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    // title: process.env.npm_package_name || '',
    title: 'Moneybar財經商業資訊社群網站',
    meta: [
      /* Seo meta tags */
      {
        hid: 'description',
        name: 'description',
        content:
          'Moneybar是一個財經商業資訊社群網站，幫助你實現財富自由人生，快樂退休。提供財商（Financial Intelligence Quotient）知識,小資理財、親子理財、個人退休金計算、ETF報價、外匯報價、投資理財規劃、最新財經新聞、研究報告、投資趨勢及投資高手實戰經驗財經新聞都在moneybar財經商業資訊社群網站。'
      },
      {
        name: 'keywords',
        content: '財經資訊網, moneybar 財經, 財經新聞, 世界財經資訊'
      },
      {
        property: 'og:locale',
        content: 'zh-TW'
      },
      {
        property: 'og:site_name',
        content: 'Moneybar財經商業資訊社群網站'
      },
      /* Required Meta Tags */
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      /* Robot meta tags */
      {
        name: 'robots',
        content: 'index, follow'
      },
      {
        name: 'googlebot',
        content: 'index, follow'
      },
      {
        name: 'google-signin-scope',
        content: 'profile email'
      },
      {
        name: 'google-signin-client_id',
        content:
          '1066052680602-3d0989h43rv1kj8ju34urr1i2l9ofbks.apps.googleusercontent.com'
      }
    ],
    script: [
      {
        hid: 'stripe',
        src: '/js/ie-detect.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/jquery/jquery.min.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/popper.js/popper.min.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/bootstrap/js/bootstrap.min.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/owl.carousel/owl.carousel.min.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/smoothscroll-for-websites/SmoothScroll.js',
        defer: true
      },
      {
        hid: 'stripe',
        src: '/vendor/bootstrap-customizer/js/bootstrap-customizer.js',
        defer: true
      },
      {
        src: 'https://apis.google.com/js/platform.js?onload=renderButton',
        defer: true,
        async: true
      },
      {
        src:
          'https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js'
      },
      {
        src: '/js/fb-sdk.js'
      },
      {
        hid: 'stripe',
        src:
          'https://cdnjs.cloudflare.com/ajax/libs/howler/2.2.1/howler.min.js',
        async: true
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      /* App manifest */
      {
        rel: 'icon',
        href: '/favicon.ico'
      },
      /* Google Fonts */
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C400i%2C600%2C700%7CRoboto%3A400%2C400i%2C500%2C700'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Abel&display=swap'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/AppSpinner.vue',
  /*
   ** Global CSS (custom css)
   */
  css: ['~assets/scss/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '@/plugins/vue-lazyload',
    '@/plugins/filter.js',
    '@/plugins/vue-inject.js',
    '@/plugins/global-components.js',
    '@/plugins/route.js',
    '@/plugins/context-inject.js',
    { src: '~plugins/ga.js', mode: 'client' },
    { src: '@/plugins/axios', mode: 'server' }
  ],
  env: {
    domain:
      process.env.NODE_ENV === 'development'
        ? 'http://localhost:3000'
        : 'https://www.moneybar.com.tw'
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/dotenv'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    '@nuxtjs/sitemap'
  ],
  sitemap: {
    hostname: 'https://www.moneybar.com.tw',
    routes() {
      return getRoutes()
    },
    exclude: [
      '/www.richkid.com.tw',
      '/cart',
      '/cart_result',
      '/cart_result_payment'
    ]
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL
  },
  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  },
  hooks: {
    generate: {
      routeCreated({ errors }) {
        if (errors.length) {
          console.error(console.error(errors))
        }
      },
      done() {
        console.log('generating success')
      }
    }
  },
  router: {
    extendRoutes(routes, resolve) {
      routes.unshift(
        {
          name: 'Plans',
          path: '/blogs/moneyvip/:id/plans/',
          component: resolve('@/pages/blogs/moneyvip/_id/plans/index.vue')
        },
        {
          name: 'PlanId',
          path: '/blogs/moneyvip/:id/plans/:planid',
          component: resolve('@/pages/blogs/moneyvip/_id/plans/_planid.vue')
        }
      )
    }
  },
  serverMiddleware: [
    { path: '/', handler: '~/server/oldRoutesRedirect.js' },
    { path: '/lineTodayvedio', handler: '~/server/lineVedioRSS.js' },
    { path: '/CK101', handler: '~/server/ck101.js' },
    { path: '/Yahoo', handler: '~/server/yahoo.js' },
    { path: '/LineToday', handler: '~/server/lineToday.js' },
    { path: '/AiVideo', handler: '~/server/aiVideo.js' }
  ]
}
