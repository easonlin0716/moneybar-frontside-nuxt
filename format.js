const obj = [
  {
    NPO: '2588555',
    name: null,
    email: null,
    phone: null,
    demand: null,
    postal: null,
    address: null,
    buyerBan: null,
    carrierId: null,
    invoiceid: 0,
    setDefalt: 'false',
    carrierType: null
  },
  {
    NPO: null,
    name: '林曉茹',
    email: 'nt1005@hotmail.com',
    phone: '0911123456',
    demand: 0,
    postal: '111',
    address: '台北市大同區',
    buyerBan: '53118277',
    carrierId: null,
    invoiceid: 1,
    setDefalt: 'true',
    carrierType: 'member'
  },
  {
    NPO: null,
    name: null,
    email: null,
    phone: null,
    demand: null,
    postal: null,
    address: null,
    buyerBan: null,
    carrierId: '/123455',
    invoiceid: 2,
    setDefalt: 'false',
    carrierType: 'cellphone'
  },
  {
    NPO: null,
    name: null,
    email: null,
    phone: null,
    demand: null,
    postal: null,
    address: null,
    buyerBan: null,
    carrierId: '12345677',
    invoiceid: 3,
    setDefalt: 'false',
    carrierType: 'citizen'
  }
]
