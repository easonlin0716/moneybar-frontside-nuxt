import Vue from 'vue'
import VueLazyLoad from 'vue-lazyload'

import loading from '@/static/img/assets/lazy-empty.png'

Vue.use(VueLazyLoad, {
  preLoad: 1,
  loading,
  attempt: 1
})
