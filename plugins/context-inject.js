import tokenGenerator from '@/utils/tokenGenerator'
import imgPlaceholder from '@/utils/imgPlaceholder'
import timeFilter from '@/utils/timeFilter'

export default (context, inject) => {
  inject('tokenGenerator', tokenGenerator)
  context.$tokenGenerator = tokenGenerator
  inject('imgPlaceholder', imgPlaceholder)
  context.$imgPlaceholder = imgPlaceholder
  inject('timeFilter', timeFilter)
  context.$timeFilter = timeFilter
}
