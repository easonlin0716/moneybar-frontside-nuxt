export default (context) => {
  context.app.router.afterEach((to, from) => {
    const exceptionRoutes = ['/login', '/register', '/forgot', '/reset']
    for (let i = 0; i < exceptionRoutes.length; i++) {
      if (to.fullPath.includes(exceptionRoutes[i])) return null
    }
    if (process.client) {
      window.scrollTo(0, 0)
      localStorage.setItem('LS_ROUTE_KEY', to.fullPath)
      return null
    }
  })
}
