import Vue from 'vue'
import AppSectionLoader from '@/components/AppSectionLoader'

const components = { AppSectionLoader }

Object.entries(components).forEach(([name, component]) => {
  Vue.component(name, component)
})
