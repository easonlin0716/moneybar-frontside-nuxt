import Vue from 'vue'
Vue.filter('stringSlicer', function(str, len) {
  if (!str) return str
  if (str.length < len) return str
  return str.slice(0, len) + '...'
})
