/*
 * 在此引入需寫入 prototype 的模組
 */

import Vue from 'vue'
import Swal from 'sweetalert2'
import { Toast } from '@/utils/Toast'

Vue.prototype.$Toast = Toast
Vue.prototype.$Swal = Swal
