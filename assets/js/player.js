/* eslint-disable */

export default function(audioInfo = {}) {
  // Cache references to DOM elements.
  var elms = [
    'progressWapper',
    'track',
    'timer',
    'duration',
    'playBtn',
    'pauseBtn',
    'progress',
    'bar',
    'loading',
    'list',
    'volume',
    'barEmpty',
    'barFull',
    'sliderBtn',
    'playerBar'
  ]
  elms.forEach(function(elm) {
    window[elm] = document.getElementById(elm)
  })
  initDuration(audioInfo.file)

  /**
   * Player class containing the state of our playlist and where we are in it.
   * Includes all methods for playing, skipping, updating the display, etc.
   * @param {Array} playlist Array of objects with playlist song details ({title, file, howl}).
   */
  var Player = function(playlist) {
    this.playlist = playlist
    this.index = 0

    // Display the title of the first track.
    track.innerHTML = playlist.title

    // Setup the playlist display.
    // playlist.forEach(function (song) {
    //   var div = document.createElement("div");
    //   div.className = "list-song";
    //   div.innerHTML = song.title;
    //   div.onclick = function () {
    //     player.skipTo(playlist.indexOf(song));
    //   };
    //   list.appendChild(div);
    // });
  }
  Player.prototype = {
    /**
     * Play a song in the playlist.
     * @param  {Number} index Index of the song in the playlist (leave empty to play the first or current).
     */
    play: function(index) {
      var self = this
      var sound

      index = typeof index === 'number' ? index : self.index
      var data = self.playlist

      // If we already loaded this track, use the current one.
      // Otherwise, setup and load a new Howl.
      if (data.howl) {
        sound = data.howl
      } else {
        sound = data.howl = new Howl({
          src: [data.file],
          html5: true, // Force to HTML5 so that the audio can stream in (best for large files).
          onplay: function() {
            // Display the duration.
            duration.innerHTML = formatTime(Math.round(sound.duration()))

            // Start upating the progress of the track.
            requestAnimationFrame(self.step.bind(self))
            pauseBtn.style.display = 'block'
          },
          onload: function() {
            loading.style.display = 'none'
          },
          onend: function() {
            moneyAudioPlayerBar.stop()
            playBtn.style.display = 'block'
            pauseBtn.style.display = 'none'
          },
          onstop: function() {
            // stop
          },
          onseek: function() {
            // Start upating the progress of the track.
            requestAnimationFrame(self.step.bind(self))
          }
        })
      }

      // Begin playing the sound.
      sound.play()

      // Update the track display.
      track.innerHTML = data.title

      // Show the pause button.
      if (sound.state() === 'loaded') {
        playBtn.style.display = 'none'
        pauseBtn.style.display = 'block'
      } else {
        loading.style.display = 'block'
        playBtn.style.display = 'none'
        pauseBtn.style.display = 'none'
      }

      // Keep track of the index we are currently playing.
      self.index = index

      // 跑馬燈
      setMarqueeOnTrack()
    },

    /**
     * Pause the currently playing track.
     */
    pause: function() {
      var self = this

      // Get the Howl we want to manipulate.
      var sound = self.playlist.howl

      // Puase the sound.
      sound.pause()

      // Show the play button.
      playBtn.style.display = 'block'
      pauseBtn.style.display = 'none'
    },

    /**
     * Skip to a specific track based on its playlist index.
     * @param  {Number} index Index in the playlist.
     */
    skipTo: function(index) {
      var self = this

      // Stop the current track.
      if (self.playlist.howl) {
        self.playlist.howl.stop()
      }

      // Reset progress.
      progress.style.width = '0%'

      // Play the new track.
      self.play(index)
    },

    /**
     * Set the volume and update the volume slider display.
     * @param  {Number} val Volume between 0 and 1.
     */
    volume: function(val) {
      var self = this

      // Update the global volume (affecting all Howls).
      Howler.volume(val)

      // Update the display on the slider.
      var barWidth = (val * 90) / 100
      barFull.style.width = barWidth * 100 + '%'
      sliderBtn.style.left =
        window.innerWidth * barWidth + window.innerWidth * 0.05 - 25 + 'px'
    },

    /**
     * Seek to a new position in the currently playing track.
     * @param  {Number} per Percentage through the song to skip.
     */
    seek: function(per) {
      var self = this

      // Get the Howl we want to manipulate.
      var sound = self.playlist.howl

      // Convert the percent into a seek position.
      if (sound.playing()) {
        sound.seek(sound.duration() * per)
      }
    },

    /**
     * The step called within requestAnimationFrame to update the playback position.
     */
    step: function() {
      var self = this

      // Get the Howl we want to manipulate.
      var sound = self.playlist.howl

      // Determine our current seek position.
      var seek = sound.seek() || 0
      timer.innerHTML = formatTime(Math.round(seek))
      progress.style.width = ((seek / sound.duration()) * 100 || 0) + '%'

      // If the sound is still playing, continue stepping.
      if (sound.playing()) {
        requestAnimationFrame(self.step.bind(self))
      }
    }
  }

  const Bar = function(barElement) {
    this.bar = barElement
    this.childNums = 50
  }
  Bar.prototype = {
    init: function() {
      for (let i = 0; i < this.childNums; i++) {
        const randomHeight = Math.random() * 60 + 20 + '%'
        this.bar.innerHTML += `<div class="player-bar__inner player-bar-${i}" style="width: ${100 /
          this
            .childNums}%; height: ${randomHeight}; background: #ccc; border: 0.5px solid #fff;"></div>`
      }
    },
    start: function() {
      for (let i = 0; i < this.childNums; i++) {
        const randomDuration = (Math.random() * 6 + 10) / 25
        this.bar.children[i].style.animationDuration = `${randomDuration}s`
        this.bar.children[i].style.animationPlayState = ''
        this.bar.children[i].style.animationIterationCount = 'infinite'
      }

      // animation-duration: ${randomDuration}s;
    },
    pause: function() {
      for (let i = 0; i < this.childNums; i++) {
        this.bar.children[i].style.animationPlayState = 'paused'
      }
    },
    stop: function() {
      for (let i = 0; i < this.childNums; i++) {
        this.bar.children[i].style.animationIterationCount = '1'
      }
    }
  }

  // Setup our new audio player class and pass it the playlist.
  var moneyAudioPlayer = new Player(audioInfo)
  window.moneyAudioPlayer = moneyAudioPlayer

  // Bind our player controls.
  playBtn.addEventListener('click', function() {
    moneyAudioPlayer.play()
    moneyAudioPlayerBar.start()
  })
  pauseBtn.addEventListener('click', function() {
    moneyAudioPlayer.pause()
    moneyAudioPlayerBar.pause()
  })
  progressWapper.addEventListener('click', function(event) {
    const progressBarLocation = progressWapper.getBoundingClientRect()
    const clickedLocationInProgress = event.clientX - progressBarLocation.x
    moneyAudioPlayer.seek(clickedLocationInProgress / progressBarLocation.width)
  })

  const moneyAudioPlayerBar = new Bar(playerBar)
  moneyAudioPlayerBar.init()
  window.moneyAudioPlayerBar = moneyAudioPlayerBar

  function setMarqueeOnTrack() {
    const trackParent = document.querySelector('#track-parent')
    if (!trackParent) return null
    if (track.scrollWidth > trackParent.clientWidth) {
      track.classList.add('track-moving')
      track.addEventListener('click', function(event) {
        track.classList.remove('track-moving')
        setTimeout(() => {
          track.classList.add('track-moving')
        }, 100)
      })
      track.addEventListener('mouseover', function(event) {
        track.classList.remove('track-moving')
      })
      track.addEventListener('mouseout', function(event) {
        track.classList.add('track-moving')
      })
    }
  }

  function formatTime(secs) {
    var minutes = Math.floor(secs / 60) || 0
    var seconds = secs - minutes * 60 || 0

    return minutes + ':' + (seconds < 10 ? '0' : '') + seconds
  }

  function initDuration(audioSrc) {
    const audio = new Audio()
    audio.setAttribute('src', audioSrc)
    audio.addEventListener(
      'canplaythrough',
      function() {
        duration.innerHTML = formatTime(Math.round(audio.duration))
      },
      false
    )
  }
}
