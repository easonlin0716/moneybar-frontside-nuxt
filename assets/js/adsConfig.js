function deviceDetector() {
  const userAgent = navigator.userAgent || navigator.vendor || window.opera

  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent)) {
    return 'Windows Phone'
  }

  if (/android/i.test(userAgent)) {
    return 'Android'
  }

  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
    return 'iOS'
  }

  return 'unknown'
}
const baseUrl = window.location.origin + '/'
const device = deviceDetector()

export default {
  appLink: {
    ad_img: '/img/ads-banner/300x250_App_下載AD.jpg',
    ad_link:
      device === 'iOS'
        ? 'https://apps.apple.com/tw/app/moneybar-賺bar-app-陪你學習一起變有錢/id1503118229'
        : 'https://play.google.com/store/apps/details?hl=zh_TW&id=com.moneybar.app'
  },
  vipSubscription: {
    ad_img: '/img/ads-banner/300x250_VIP訂閱AD.jpg',
    ad_link: baseUrl + 'vip'
  },
  USStock: {
    ad_img: '/img/ads-banner/300x250_美股迷AD.jpg',
    ad_link: baseUrl + 'blogs/moneyvip/59/plans'
  },
  topic: {
    ad_img: '/img/ads-banner/300x250_專題AD.jpg',
    ad_link: baseUrl + 'newsbar/topic'
  },
  financeBanner: {
    ad_img: '/img/ads-banner/728x90_youtube_AD_尬理財.jpg',
    ad_link: baseUrl + 'blogs/moneyvip/7'
  },
  vipSubscriptionBanner: {
    ad_img: '/img/ads-banner/728x90_VIP_訂閱AD.jpg',
    ad_link: baseUrl + 'vip'
  },
  missHuaBanner: {
    ad_img: '/img/ads-banner/728x90_youtube_AD_嗑財經.jpg',
    ad_link: baseUrl + 'videobar/misshua'
  },
  DBS_US11: {
    ad_img: '/img/ads-banner/DBS_US11.jpg',
    ad_link:
      'https://www.dbs.com.tw/treasures-zh/dbs-forms/depositfcy/subpage1.page?referralCode=US11mbw&utm_source=moneybar&utm_medium=display&utm_campaign=iwealth&utm_term=iwealth_US11mbw'
  },
  DBS_US12: {
    ad_img: '/img/ads-banner/DBS_US12.jpg',
    ad_link:
      'https://www.dbs.com.tw/treasures-zh/dbs-forms/depositfcy/subpage1.page?referralCode=US12mbw&utm_source=moneybar&utm_medium=display&utm_campaign=iwealth&utm_term=iwealth_US12mbw'
  },
  Parents_AD: {
    ad_img: '/img/ads-banner/Parents_AD.png',
    ad_link:
      'https://elearning.parenting.com.tw/courses/69?utm_source=maja.groups&utm_medium=social&utm_campaign=cp-video_audio-210112_%E9%A6%AC%E5%93%88%E7%90%86%E8%B2%A1%E8%AA%B2_%E9%A6%AC%E5%93%88%E7%A4%BE%E5%9C%98-210112'
  },
  Parents_AD_02: {
    ad_img: '/img/ads-banner/Parents_AD_02.png',
    ad_link:
      'https://elearning.parenting.com.tw/courses/69?utm_source=maja.groups&utm_medium=social&utm_campaign=cp-video_audio-210112_%E9%A6%AC%E5%93%88%E7%90%86%E8%B2%A1%E8%AA%B2_%E9%A6%AC%E5%93%88%E7%A4%BE%E5%9C%98-210112'
  },
  richkid_ad_1: {
    ad_img: '/img/ads-banner/richkid-300x250-1.png',
    ad_link: 'https://www.richkid.com.tw/article/165'
  },
  richkid_ad_2: {
    ad_img: '/img/ads-banner/richkid-300x250-2.png',
    ad_link: 'https://www.richkid.com.tw/article/165'
  },
  richkid_ad_3: {
    ad_img: '/img/ads-banner/richkid-1140x140-bordered.png',
    ad_link: 'https://www.richkid.com.tw/article/165'
  }
}
