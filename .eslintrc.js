module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
    jquery: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    'nuxt/no-cjs-in-config': 'off',
    'vue/attributes-order': 'off',
    'vue/attribute-hyphenation': 0,
    'vue/no-v-html': 'off',
    'no-console': 'off',
    'no-undef': 'off',
    'prettier/prettier': [
      2,
      {
        endOfLine: 'auto'
      }
    ]
  }
}
